package swing;

import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;



public class Fenetre extends JFrame implements ActionListener{
	
	public static int compteur;
	private TextField texte;
	private JLabel text;
	public static int maximum =25;
	public static int minimum =-5;
	private JLabel actuinter;
	public static TextField textinter;
	


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Fenetre(){
		new JFrame();
		this.setVisible(true);
		this.setLayout(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(400,300);
		this.setTitle("Projet swing PCD");
		
		JMenuBar barreMenus = new JMenuBar();
		
		JMenu fichier =new JMenu("Fichier");
		JMenu modifier = new JMenu("Modifier");
		JMenu intervalle = new JMenu("Intervalle");
		JMenuItem quitter = new JMenuItem("Quitter");
		JMenuItem min = new JMenuItem("D�finir min");
		JMenuItem incr�menter = new JMenuItem("+1");
		JMenuItem d�cr�menter = new JMenuItem("-1");
		JMenuItem max = new JMenuItem ("D�finir max");
		
		d�cr�menter.addActionListener(this);
		incr�menter.addActionListener(this);
		d�cr�menter.setActionCommand("-1");
		incr�menter.setActionCommand("+1");
		quitter.setActionCommand("Quitter");
		min.setActionCommand("D�finir min");
		max.setActionCommand("D�finir max");
		min.addActionListener(this);
		max.addActionListener(this);
		quitter.addActionListener(this);
		
		
		JButton bouton1 = new JButton("+1");
		bouton1.setBounds(220,120,60,20);
		bouton1.setVisible(true);
		bouton1.addActionListener(this);
		
		JButton bouton3 = new JButton("-1");
		bouton3.setBounds(70,120,60,20);
		bouton3.setVisible(true);
		bouton3.addActionListener(this);
		
		actuinter= new JLabel("intervalle actuel : ");
		actuinter.setVisible(true);
		actuinter.setBounds(50,20,150,20);
		
		textinter= new TextField(minimum + "     " + maximum);
		textinter.setVisible(true);
		textinter.setBounds(220,20,80,20);
		
		text = new JLabel("compteur actuel :");
		text.setVisible(true);
		text.setBounds(50,50,100,20);
		
		texte = new TextField("0");
		texte.setVisible(true);
		texte.setBounds(170,50,160,20);
		
		JButton bouton2 = new JButton("Reset compteur");
		bouton2.setBounds(100,200,150,20);
		bouton2.setVisible(true);
		bouton2.addActionListener(this);
		
		this.add(bouton2);
		this.add(bouton1);
		this.add(texte);
		this.add(text);
		this.add(bouton3);
		this.add(actuinter);
		this.add(textinter);
		
		fichier.add(quitter);
		modifier.add(d�cr�menter);
		modifier.add(incr�menter);
		intervalle.add(min);
		intervalle.add(max);
		barreMenus.add(fichier);
		barreMenus.add(modifier);
		barreMenus.add(intervalle);
		this.setJMenuBar(barreMenus);
		barreMenus.setVisible(true);
	}
	
	
	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand().equals("+1")){
			if(compteur<maximum){
				Fenetre.compteur++;
				texte.setText("" + compteur);
			}else{
				texte.setText(maximum +" : maximum atteint");
			}
			
		}else{if(e.getActionCommand().equals("Reset compteur")){
			compteur=0;
			texte.setText("" + compteur);
			}else{
				if(e.getActionCommand().equals("-1")){
					if(compteur>minimum){
						compteur--;
						texte.setText("" + compteur);
					}else{
						texte.setText(minimum + " : minimum atteint");
					}
				}else{
					if(e.getActionCommand().equals("D�finir min")){
						new Intervalle("D�finir min");
						
						
					}else{
						if(e.getActionCommand().equals("D�finir max")){
						new Intervalle("D�finir max");
						}else{
							if(e.getActionCommand().equals("Quitter")){
								System.exit(0);
							}
						}
					}
				}
			}
		}	
	}
		

		  
		
	
	public static void main(String args[]){	
			
			new Fenetre();
			
	}
	
	
}
