package swing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class Intervalle extends JFrame implements ActionListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private JTextField valeur;

	public Intervalle(String minOuMax){
		
		new JFrame();
		this.setVisible(true);
		this.setLayout(null);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setSize(300,200);
		this.setTitle(minOuMax);
		
		JLabel text1 = new JLabel("Nouvelle valeur : ");
		text1.setVisible(true);
		text1.setBounds(20,30,150,20);
		
		
		valeur = new JTextField();
		valeur.setVisible(true);
		valeur.setBounds(190,30,70,20);
		
		JButton bouton1 = new JButton("Valider");
		bouton1.setVisible(true);
		bouton1.setBounds(110,80,80,20);
		bouton1.addActionListener(this);
		
		this.add(text1);
		this.add(valeur);
		this.add(bouton1);
		
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand().equals("Valider")){
			if(this.getTitle()=="D�finir min"){
			Fenetre.minimum = Integer.parseInt(valeur.getText());
			Fenetre.textinter.setText(Fenetre.minimum + "     "+ Fenetre.maximum);
			dispose();
		}else{
			Fenetre.maximum = Integer.parseInt(valeur.getText());
			Fenetre.textinter.setText(Fenetre.minimum + "     "+ Fenetre.maximum);
			dispose();
			}
		}
	}
	
}
